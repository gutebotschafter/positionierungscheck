const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./common');

const { join, resolve } = require('path');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
    entry: {
        app: [
            join(__dirname, '../src/app'),
            resolve(__dirname, '../assets/styles/general.scss')
        ],
        criticals: resolve(__dirname, '../assets/styles/criticals.scss'),
    },
    mode: 'production',
    devtool: 'source-map',
    plugins: [
        new UglifyJsPlugin({
            sourceMap: true,
            parallel: true
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
    ]
});
