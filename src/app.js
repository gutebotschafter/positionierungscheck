import './polyfills';

import * as contentScroll from './components/content-scroll';
import * as postionCheck from './components/position-check';

contentScroll.init();
postionCheck.init();
