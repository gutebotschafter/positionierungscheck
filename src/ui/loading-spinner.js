import htmlToElement from 'html-to-element';

/**
 * Returns the loading spinner dom
 * @type {Object|Object[]}
 */
export const loadingSpinner = htmlToElement(
    `<div class="lds-roller cloaked"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>`
);
