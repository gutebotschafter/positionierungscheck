<?php

return [
    'debug' => true,
    'mgfagency.twig.cache' => false,
    'mgfagency.twig.function.inlineRenderer' => 'inlineRenderer',
    'email' => [
        'transport' => [
            'type' => 'smtp',
            'host' => 'smtp.office365.com',
            'port' => 587,
            'auth' => true,
            'security' => 'tls',
            'username' => 'noreply@gute-botschafter.de',
            'password' => 'Puc79480'
        ]
    ],
    "token" => "fc01c255707ba0f0ccc029e68fb232783dbdab99",
    "environments" => [
        "production" => "https://www.positionierungscheck.de",
        "stage" => "https://gb-positionierungscheck.projektstatus.de",
        "virt" => "http://localhost"
    ]
];
