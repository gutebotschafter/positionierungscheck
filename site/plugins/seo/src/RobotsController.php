<?php

namespace GutebBotschafter\Seo;

use Kirby\Http\Response;
use Kirby\Http\Server;

/**
 * Class MailController
 * @package GutebBotschafter\PositionCheck
 */
class RobotsController
{
    /** @var */
    private $ignore;

    /**
     * RobotsController constructor.
     * @param $ignore
     */
    public function __construct($ignore)
    {
        $this->ignore = $ignore;
    }

    /**
     * @return string
     */
    private function getDisallowed() : string
    {
        $txt = "User-agent: *\n\n";

        foreach ($this->ignore as $page) {
            $txt .= "Disallow: /" . $page . "\n";
        }

        $txt .= "\nSitemap: " . (Server::https() ? "https" : "http") . "://" . Server::host() . "/sitemap.xml";

        return $txt;
    }

    /**
     * Shows the textfile
     *
     * @return Response
     */
    public function show() : Response
    {
        return new Response($this->getDisallowed(), "text/plain");
    }
}
