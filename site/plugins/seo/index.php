<?php

use GutebBotschafter\Seo\SitemapController;
use GutebBotschafter\Seo\RobotsController;

load([
    'GutebBotschafter\\Seo\\SitemapController' => __DIR__ . DS . 'src' . DS . 'SitemapController.php',
    'GutebBotschafter\\Seo\\RobotsController' => __DIR__ . DS . 'src' . DS . 'RobotsController.php'
]);

Kirby::plugin("gb-kirby/sitemap", [
    "routes" => function () {
        $ignore = kirby()->option("seo.ignore", [
            "error",
            "helpsection/contenttypes",
            "helpsection/contenttypes/text",
            "helpsection/contenttypes/redirect",
            "helpsection/text",
            "helpsection/text/intro",
            "helpsection/text/formats",
            "helpsection/text/formats/bold",
            "helpsection/text/formats/headlines"
        ]);

        return [
            [
                'pattern' => 'sitemap.xml',
                'action'  => function () use (&$ignore) {
                    $sitemap = new SitemapController($ignore);
                    return $sitemap->show();
                }
            ], [
                'pattern' => 'robots.txt',
                'action'  => function () use (&$ignore) {
                    $robots = new RobotsController($ignore);

                    return $robots->show();
                }
            ]
        ];
    }
]);
