<?php

use GutebBotschafter\PositionCheck\MailController;
use GutebBotschafter\PositionCheck\PageController;

load([
    'GutebBotschafter\\PositionCheck\\MailController' => __DIR__ . DS . 'src' . DS . 'MailController.php',
    'GutebBotschafter\\PositionCheck\\PageController' => __DIR__ . DS . 'src' . DS . 'PageController.php',
    'GutebBotschafter\\PositionCheck\\AbstractEnum' => __DIR__ . DS . 'src' . DS . 'AbstractEnum.php',
    'GutebBotschafter\\PositionCheck\\Constants' => __DIR__ . DS . 'src' . DS . 'Constants.php'
]);

Kirby::plugin('gb/position-check', [
    'blueprints' => [
        'pages/reports' => __DIR__ . '/blueprints/pages/reports.yml',
        'pages/questions' => __DIR__ . '/blueprints/pages/questions.yml',
        'pages/question' => __DIR__ . '/blueprints/pages/question.yml',
        'pages/results' => __DIR__ . '/blueprints/pages/results.yml',
        'pages/result' => __DIR__ . '/blueprints/pages/result.yml'
    ],
    'templates' => [
        'reports' => __DIR__ . '/templates/reports.twig',
        'results' => __DIR__ . '/templates/results.twig',
        'result' => __DIR__ . '/templates/result.twig'
    ],
    'controllers' => [
        'reports' => require 'controllers/reports.php',
        'results' => require 'controllers/results.php',
        'result' => require 'controllers/result.php'
    ],
    "routes" => function ($kirby) {
        return [
            [
                "pattern" => "/gb/mailing",
                "method" => "GET|POST",
                "action" => function () use (&$kirby) {
                    $mail = new MailController($kirby);

                    return $mail->send(false);
                }
            ], [
                "pattern" => "/gb/notify",
                "method" => "GET|POST",
                "action" => function () use (&$kirby) {
                    $mail = new MailController($kirby);

                    return $mail->send(true);
                }
            ], [
                "pattern" => "/gb/pages",
                "method" => "GET|POST",
                "action" => function () use (&$kirby) {
                    $page = new PageController($kirby);

                    return $page->create();
                }
            ]
        ];
    }
]);
