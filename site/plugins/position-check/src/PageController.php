<?php

namespace GutebBotschafter\PositionCheck;

use Kirby\Cms\App;
use Kirby\Cms\Page;
use Kirby\Exception\Exception;
use Kirby\Http\Response;
use Kirby\Toolkit\Str;

/**
 * Class PageController
 * @package GutebBotschafter\PositionCheck
 */
class PageController
{
    /** @var */
    private $kirby;

    /**
     * Sets the kirby app instance
     *
     * @param App $kirby
     */
    public function __construct(App $kirby)
    {
        $this->kirby = $kirby;
    }

    /**
     * Searches for the result page
     *
     * @return Page|void
     */
    public function getResultsPage()
    {
        foreach ($this->kirby->site()->children() as $page) {
            if ($page->template()->name() == Constants::RESULTS_DIRECTORY) {
                return $page;
            }
        }

        return;
    }

    /**
     * Creates the results page holder
     *
     * @return Page
     */
    public function createResultsPage(): Page
    {
        return $this->kirby->site()->createChild([
            "slug" => Constants::RESULTS_DIRECTORY,
            "template" => "results",
            "content" => [
                "title" => Constants::HEADLINE_OVERVIEW
            ]
        ]);
    }

    /**
     * Creates the result page
     *
     * @param Page $page
     * @param array $props
     * @return Page
     */
    public function createResultPage(Page $page, array $props): Page
    {
        return $page->createChild([
            "slug" => $props["slug"],
            "template" => "result",
            "status" => "unlisted",
            "content" => [
                "title" => Constants::REPORT_TITLE . " - " .
                    $props["mailConfig"]["page"] . " - "
                    . date("d.m.Y H:i"),
                "container" => $props["container"],
                "mailConfig" => $props["mailConfig"]
            ]
        ]);
    }

    /**
     * Returns the json
     *
     * @param array $props
     * @return Response
     */
    public function response(array $props): Response
    {
        return new Response(json_encode($props), "application/json", $props["code"]);
    }

    /**
     * Changes the status of the page
     *
     * @param Page $page
     * @param string $status
     * @return Page|Response
     */
    public function changeStatus(Page $page, string $status)
    {
        try {
            $page = $page->changeStatus($status);
        } catch (Exception $exception) {
            $props = [
                "status" => "failed",
                "message" => $exception->getMessage(),
                "code" => 400
            ];

            return $this->response($props);
        }

        return $page;
    }

    /**
     * Creates the page from the check
     *
     * @return Response
     */
    public function create(): Response
    {
        $result = $this->kirby->request()->body()->toArray();

        $this->kirby->impersonate('kirby'); // Important for create pages!

        if (!array_key_exists("container", $result)) {
            $props = [
                "status" => "failed",
                "code" => 400
            ];

            return $this->response($props);
        }

        $slug = str::slug(time());
        $props = [
            "container" => $result["container"],
            "mailConfig" => $result["mailConfig"],
            "slug" => $slug
        ];

        $page = $this->getResultsPage();

        if (!($page instanceof Page)) {
            $page = $this->createResultsPage();
        }

        $page = $this->changeStatus($page, "unlisted");

        $page = $this->createResultPage($page, $props);
        $this->changeStatus($page, "unlisted");

        $props = [
            "status" => "success",
            "data" => [
                "slug" => Constants::RESULTS_DIRECTORY . "/" . $slug
            ],
            "code" => 200
        ];

        return $this->response($props);
    }
}
