<?php

namespace GutebBotschafter\PositionCheck;

use AbstractEnum;

/**
 * Class Constants
 * @package GutebBotschafter\PositionCheck
 */
class Constants extends AbstractEnum
{
    const ERROR_GENERAL = "Es ist ein Fehler aufgetreten. Bitte probieren Sie es noch einmal.";

    const ERROR_WRONG_MAIL = "Bitte geben Sie eine gültige E-Mail Adresse ein";

    const HEADLINE_OVERVIEW = "Reports";

    const REPORT_TITLE = "Report";

    const HEADLINE_REPORTS_OVERVIEW = "Reports";

    const RESULTS_DIRECTORY = "results";
}
