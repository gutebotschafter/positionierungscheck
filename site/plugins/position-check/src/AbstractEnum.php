<?php

/**
 * Class AbstractEnum
 */
abstract class AbstractEnum
{
    /**
     * @return array
     */
    public static function getConstants()
    {
        $rc = null;

        try {
            $rc = new \ReflectionClass(get_called_class());
        } catch (ReflectionException $e) {
        }

        return $rc->getConstants();
    }

    /**
     * @return array
     */
    public static function lastConstants()
    {
        $parentConstants = static::getParentConstants();

        $allConstants = static::getConstants();

        return array_diff($allConstants, $parentConstants);
    }

    /**
     * @return array
     */
    public static function getParentConstants()
    {
        $rc = null;

        try {
            $rc = new \ReflectionClass(get_parent_class(static::class));
        } catch (ReflectionException $e) {
        }

        $consts = $rc->getConstants();

        return $consts;
    }
}
