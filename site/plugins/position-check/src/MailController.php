<?php

namespace GutebBotschafter\PositionCheck;

use Kirby\Cms\App;
use Kirby\Exception\Exception;
use Kirby\Http\Response;

/**
 * Class MailController
 * @package GutebBotschafter\PositionCheck
 */
class MailController
{
    /** @var */
    private $kirby;

    /**
     * MailController constructor.
     * @param App $kirby
     */
    public function __construct(App $kirby)
    {
        $this->kirby = $kirby;
    }

    /**
     * Returns the json
     *
     * @param array $props
     * @return Response
     */
    public function response(array $props): Response
    {
        return new Response(json_encode($props), "application/json", $props["code"]);
    }

    /**
     * Sends the mail for contact form
     *
     * @param bool|null $notify
     * @return Response
     */
    public function send(bool $notify = null): Response
    {
        $result = $this->kirby->request()->body()->toArray();
        $mailConfig = $result["mailConfig"];

        $page = page($mailConfig["page"]);

        if (!$page) {
            $props = [
                "status" => "failed",
                "message" => Constants::ERROR_GENERAL,
                "code" => 400
            ];

            return $this->response($props);
        }

        $data = [
            "email" => $mailConfig["mailTo"],
            "name" => $mailConfig["name"],
        ];
        $rules = [
            "email" => ["required", "email"],
        ];
        $messages = [
            "email" => Constants::ERROR_WRONG_MAIL,
        ];

        if (invalid($data, $rules, $messages) && !$notify) {
            $props = [
                "status" => "failed",
                "message" => $messages["email"],
                "code" => 400
            ];

            return $this->response($props);
        }

        $link = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . "/" . $mailConfig["slug"];

        $template = $page->content()->mailText()->value;
        $template = str_replace("%link%", $link, $template);
        $template = strlen($mailConfig["name"]) > 0
            ? str_replace("%name%", $mailConfig["name"], $template)
            : str_replace(" %name%", "", $template);

        $email = $page->content()->mail()->value;

        $mail = [
            "from" => $email,
            "to" => $notify ? $email : $mailConfig["mailTo"],
            "subject" => $page->content()->mailSubject()->value,
            "body" => $template
        ];

        if (!$notify) {
            $mail = array_merge($mail, [
                "bcc" => $email
            ]);
        }

        try {
            $this->kirby->email($mail);
        } catch (Exception $exception) {
            $props = [
                "status" => "failed",
                "message" => $exception->getMessage(),
                "code" => 500
            ];

            return $this->response($props);
        }

        $props = [
            "status" => "success",
            "code" => 200
        ];

        return $this->response($props);
    }
}
