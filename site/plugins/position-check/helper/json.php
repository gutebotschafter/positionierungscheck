<?php

/**
 * Converts an array to a json html attribute
 *
 * @param $json
 * @return string
 */
function array2jsonAttribute($json): string
{
    return str_replace("\u0022", "\\\\\"", json_encode($json, JSON_HEX_QUOT));
}
