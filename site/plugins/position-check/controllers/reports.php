<?php

require_once __DIR__ . "/../helper/json.php";
require_once __DIR__ . "/../helper/hash.php";

require_once __DIR__ . "/../src/AbstractEnum.php";
require_once __DIR__ . "/../src/Constants.php";

use \GutebBotschafter\PositionCheck\Constants;

return function ($page): array {
    $json = [
        "config" => [
            "introHeadline" => $page->content()->introheadline()->kt()->value,
            "introText" => $page->content()->introtext()->kt()->value,
            "length" => $page->content()->length()->value,
            "type" => $page->content()->type()->value,
            "style" => $page->content()->style()->value,
            "mininumRateDescription" => $page->content()->mininumratedescription()->value,
            "maximumRateDescription" => $page->content()->maximumratedescription()->value,
            "startingRate" => 1,
            "resultHeadline" => $page->content()->resultheadline()->value,
            "resultTextHeadline" => $page->content()->resulttextheadline()->value,
            "resultText" => explode("|", $page->content()->resulttext()->value),
            "questionTotalCount" => 0,
            "ratingsTotalCount" => 0.0,
            "maxQuestionPoints" => 0,
            "ctaText" => $page->content()->ctatext()->value,
            "finishText" => $page->content()->finishText()->value,
            "buttonTextFinish" => $page->content()->buttontextfinish()->value,
            "mailSubject" => $page->content()->mailsubject()->value,
            "mailText" => $page->content()->mailtext()->value,
            "email" => $page->content()->mail()->value
        ],
        "container" => [],
        "constants" => []
    ];

    foreach (Constants::getConstants() as $key => $value) {
        $json["constants"] = array_merge($json["constants"], [
            $key => $value
        ]);
    }

    $parents = $page->children();

    foreach ($parents as $parent) {
        $container = [
            "headline" => $parent->content()->title()->value,
            "maxProgress" => 0.0,
            "questionProgress" => 0.0,
            "questionProgressTick" => 0.0,
            "ratingCount" => 0,
            "maxRatingCount" => 0,
            "totalResultPercent" => 0.0,
            "questions" => [],
            "result" => explode("|", $parent->content()->resulttext()->value)
        ];

        foreach ($parent->children() as $child) {
            $container["questions"][] = [
                "question" => $child->content()->headline()->value,
                "value" => null,
                "identifier" => generateRandomString(15)
            ];
        }

        $container["questionProgressTick"] = (float)(100 / count($container["questions"]));
        $container["maxProgress"] = (float)(100 / count($parents));
        $container["maxRatingCount"] = count($container["questions"]) * $json["config"]["length"];

        $json["config"]["questionTotalCount"] = $json["config"]["questionTotalCount"] + count($container["questions"]);
        $json["container"][] = $container;
    }

    $json["config"]["maxQuestionPoints"] = $json["config"]["questionTotalCount"] * $json["config"]["length"];

    return [
        "json" => array2jsonAttribute($json),
        "config" => $json["config"],
        "constants" => $json["constants"]
    ];
};
