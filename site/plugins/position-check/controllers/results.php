<?php

require_once __DIR__ . "/../src/AbstractEnum.php";
require_once __DIR__ . "/../src/Constants.php";

use \GutebBotschafter\PositionCheck\Constants;

return function ($page): array {
    $children = $page->children();
    $props = [
        "props" => [],
        "constants" => []
    ];

    foreach ($children as $child) {
        $props["props"][] = [
            "title" => $child->content()->title()->toString(),
            "url" => $child->url()
        ];
    }

    foreach (Constants::getConstants() as $key => $value) {
        $props["constants"] = array_merge($props["constants"], [
            $key => $value
        ]);
    }

    return $props;
};
