<?php

return function ($page): array {
    $fields = $page->content()->fields();

    $props = [
        "container" => $fields["container"]->yaml(),
        "mailConfig" => $fields["mailconfig"]->yaml()
    ];

    return $props;
};
