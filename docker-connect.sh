#!/usr/bin/env bash

if [[ $1 == "" ]]; then
    echo "Missing Container Parameter like php or web"
    exit
fi

DOCKERID=$(docker ps | grep $1 | cut -d' ' -f1)

docker exec -it "$DOCKERID" /bin/bash
